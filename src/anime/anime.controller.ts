import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { AnimeService } from './anime.service';

@Controller('anime')
export class AnimeController {
  constructor(private readonly animeService: AnimeService) {}

  @Get('/search')
  async search(@Query('title') title) {
    return await this.animeService.search(title);
  }

  @Post('/add')
  async addToList(@Body() body: any) {
    return await this.animeService.addToList(body.animeID, body.customerID);
  }
}

