import { Logger, Module } from '@nestjs/common';
import { AnimeController } from './anime.controller';
import { AnimeService } from './anime.service';
import { Anime, AnimeSchema } from 'src/schemas/Anime/anime.schema';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import {
  CustomerAnime,
  CustomerAnimeSchema,
} from 'src/schemas/CustomerAnime/customer-anime.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Anime.name, schema: AnimeSchema },
      { name: CustomerAnime.name, schema: CustomerAnimeSchema },
    ]),
    HttpModule,
  ],
  controllers: [AnimeController],
  providers: [AnimeService],
  exports: [AnimeModule],
})
export class AnimeModule {}
