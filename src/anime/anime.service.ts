import { HttpService } from '@nestjs/axios';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Anime } from 'src/schemas/Anime/anime.schema';
import * as dotenv from 'dotenv';
import { firstValueFrom } from 'rxjs';
import { CustomerAnime } from 'src/schemas/CustomerAnime/customer-anime.schema';
dotenv.config();

// dotenv.config();
// const assetPath = process.cwd() + path.sep + 'asset' + path.sep;
// const url = 'http://localhost:4300/manga/mangahere/info';
const url = process.env.ANIME_API;
export interface AnimeResponse {
  pagination: any;
  data: Anime[];
}

@Injectable()
export class AnimeService {
  constructor(
    @InjectModel(Anime.name) private readonly productModel: Model<Anime>,
    @InjectModel(CustomerAnime.name)
    private readonly customerAnime: Model<CustomerAnime>,
    private readonly httpService: HttpService,
  ) {}

  async addToList(animeID: number, customerID: string): Promise<CustomerAnime> {
    const { data } = await firstValueFrom(
      await this.httpService.get(url + `/anime/${animeID}`, {}),
    );
    if (!(await this.find(animeID))) await this.productModel.create(data.data);
    const currentEP = await this.getLatestEpisode(animeID);
    return await this.customerAnime.create({
      animeID,
      animeTitle: data.data.title,
      animeTitleEng: data.data.title_english,
      customerID,
      currentEP,
    });
  }
  async getLatestEpisode(animeID: number) {
    const { data } = await firstValueFrom(
      await this.httpService.get(url + `/anime/${animeID}/episodes`, {}),
    );
    return data.data.length;
  }

  async find(mal_id: number) {
    return await this.productModel
    .findOne({ mal_id }).exec();
  }

  async search(title: string) {
    const { data } = await firstValueFrom(
      await this.httpService.get(url + '/anime', {
        params: { q: title },
      }),
    );
    const anime: AnimeResponse = data.data;
    // console.log(data.data[0]);
    return data.data.splice(0, 5);
    // throw new Error('Method not implemented.');
  }

  async getAllCustomerAnime() {
    return await this.customerAnime.find().exec();
  }

  async checkNewEpisode(customerID, animeID) {
    const customerAnime: CustomerAnime = await this.customerAnime
      .findOne({ customerID, animeID })
      .exec();
    const latest = await this.getLatestEpisode(animeID);
    if (latest > customerAnime.currentEP) {
      await this.updateEpisode(customerID, animeID, latest);
      // customerAnime.currentEP = latest;
      // await customerAnime.save();
      return { res: true, data: { customerAnime, latest } };
    }
    return { res: false, data: {}};
  }
  async updateEpisode(customerID: any, animeID: any, latest: any) {
    await this.customerAnime.updateOne(
      { customerID, animeID },
      { currentEP: latest },
    );
  }
}
