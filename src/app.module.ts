import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CustomerModule } from './customer/customer.module';
import { ProductModule } from './product/product.module';
import { BoxModule } from './box/box.module';
import * as dotenv from 'dotenv';
import { CustomerService } from './customer/customer.service';
import { TelegramModule } from './telegram/telegram.module';
import { AnimeModule } from './anime/anime.module';
dotenv.config();

@Module({
  imports: [
    AuthModule,
    MongooseModule.forRoot(
      `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}?authSource=admin`,
      {
        // local
        // useCreateIndex: true,
        // useNewUrlParser: true,
        // useUnifiedTopology: true,
      },
    ),
    CustomerModule,
    ProductModule,
    BoxModule,
    TelegramModule,
    AnimeModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
