import { Inject, Injectable, Logger } from '@nestjs/common';
import { CustomerService } from './customer/customer.service';
import { ProductService } from './product/product.service';
import { BoxService } from './box/box.service';
import { Box } from './schemas/Box/box.schema';
import { CustomerProduct } from './schemas/CustomerProduct/customer-product.schema';
import { Product } from './schemas/Product/product.schema';
import { Customer } from './schemas/Customer/customer.schema';
import { TelegramService } from './telegram/telegram.service';
import { AnimeService } from './anime/anime.service';
import { CustomerAnime } from './schemas/CustomerAnime/customer-anime.schema';

@Injectable()
export class AppService {
  async setCronJob(
    customerService: CustomerService,
    animeService: AnimeService,
    productService: ProductService,
    boxService: BoxService,
    telegramService: TelegramService,
  ) {
    Logger.verbose('Cron job');
    const customersToNotify: CustomerAnime[] =
      await animeService.getAllCustomerAnime();
    for (const customerToNotify of customersToNotify) {
      const { res, data } = await animeService.checkNewEpisode(
        customerToNotify.customerID,
        customerToNotify.animeID,
      );
      if (res)
        await telegramService.notify(
          data.customerAnime.customerID,
          data.customerAnime.animeTitleEng
            ? data.customerAnime.animeTitleEng
            : data.customerAnime.animeTitle,
          data.latest,
        );
    }
    // const mangaTitleUpdated = await productService.getLatestVolumes();
    // const boxes: Box[] = await boxService.findAll();
    // for (const box of boxes) {
    //   const customerProd: CustomerProduct = box.customerProducts.find((cProd) =>
    //     mangaTitleUpdated.map((m) => m.title).includes(cProd.product.title),
    //   );
    //   if (customerProd) {
    //     Logger.log(`Notifyng ${box.customer} of `);
    //     Logger.log(customerProd.product.title);
    //     const latest = mangaTitleUpdated.find(
    //       (m) => m.title === customerProd.product.title,
    //     );
    //     Logger.log(latest);
    //     const c: Customer = await customerService.findById(
    //       String(box.customer),
    //     );
    //     telegramService.notify(c, latest, customerProd.product.image);
    //   }
    // }
  }
  getHello(): string {
    return 'Hello World!';
  }
}
