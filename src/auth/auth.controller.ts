import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../schemas/User/user.dto';
import { User } from '../schemas/User/user.schema';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  init(funName: string, body?: any) {
    // Logger.warn(`Calling ${this.init.caller.name} with: `);
    Logger.debug(`Calling ${funName}`);
    Logger.debug(` with: ${body}`);
  }

  @Post('/login')
  async login(@Body() loginUser: CreateUserDto) {
    this.init(this.login.name, loginUser);
    const user = await this.authService.findOne(loginUser.username);
    console.log(loginUser);
    
    const res: boolean = this.authService.validateUser(loginUser, user);
    Logger.debug(`Confirmed: ${res}`);
    return res ? { username: user.username } : undefined;
  }

  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<User> {
    this.init(this.create.name, createUserDto);
    return this.authService.create(createUserDto);
  }

  @Get()
  async findAll(): Promise<User[]> {
    this.init(this.findAll.name);
    return this.authService.findAll();
  }

  @Get(':nick')
  async findOne(@Param('nick') nick: string): Promise<User> {
    this.init(this.findOne.name, nick);
    return this.authService.findOne(nick);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    this.init(this.delete.name, id);
    return this.authService.delete(id);
  }
}
