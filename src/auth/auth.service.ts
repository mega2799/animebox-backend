import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../schemas/User/user.schema';
import { CreateUserDto } from '../schemas/User/user.dto';
import { sha512 } from 'js-sha512';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
  ) {}

  //TODO md5 password encr
  public validateUser(loginUser: CreateUserDto, user: User) {
    return (
      loginUser.username === user.username &&
      loginUser.password === user.password
    );
  }

  async create(createCatDto: CreateUserDto): Promise<User> {
    console.log(createCatDto.username);
    const exists = await this.findOne(createCatDto.username);
    console.log(exists);
    // createCatDto.password = sha512(createCatDto.password);
    if (!exists) {
      console.log('lol');
      const createdCat = await this.userModel.create(createCatDto);
      return createdCat;
    }
    throw new HttpException('Conflict', HttpStatus.CONFLICT);
  }

  async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async findOne(nick: string): Promise<User> {
    return this.userModel.findOne({ username: nick }).exec();
  }

  async delete(id: string) {
    const deletedCat = await this.userModel
      .findByIdAndRemove({ _id: id })
      .exec();
    return deletedCat;
  }
}
