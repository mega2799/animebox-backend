import { Test, TestingModule } from '@nestjs/testing';
import { BoxService } from './box.service';
import { MongooseModule, getModelToken } from '@nestjs/mongoose';
import { Box, BoxSchema } from '../schemas/Box/box.schema';
import { BoxController } from './box.controller';
import { Connection, Model, connect } from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import {
  Customer,
  CustomerDocument,
  CustomerSchema,
} from '../schemas/Customer/customer.schema';
import { CustomerProduct } from '../schemas/CustomerProduct/customer-product.schema';
import { CustomerController } from '../customer/customer.controller';
import { HttpModule } from '@nestjs/axios';
import { CustomerService } from '../customer/customer.service';
import { Logger } from '@nestjs/common';
import { ProductController } from '../product/product.controller';
import { Product, ProductSchema } from '../schemas/Product/product.schema';
import { ProductService } from '../product/product.service';
import { Manga, MangaSchema } from '../schemas/Manga/manga.schema';

const giovanni: Customer = {
  CF: 'mttdggvvv',
  name: 'Giovanni',
  surname: 'Di giovanni',
  telegramID: 'ksajdbf329db',
  email: 'digio@gmail.com',
  image:
    'https://images.pexels.com/photos/2698519/pexels-photo-2698519.jpeg?auto=compress&cs=tinysrgb&w=1600',
};

const callOfTheNight: CustomerProduct = {
  product: {
    title: 'yofukashi_no_uta',
  },
  currentID: 0,
};

// const giovanniBox: Box = {
//   customer: giovanni,
//   customerProducts: [
//     {
//       product: callOfTheNight.product,
//       currentID: 0,
//     },
//   ],
// };

describe('BoxController', () => {
  let controllerBox: BoxController;
  let customerController: CustomerController;
  let productController: ProductController;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let boxModel: Model<Box>;
  let customerModel: Model<Customer>;
  let productModel: Model<Product>;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    boxModel = mongoConnection.model(Box.name, BoxSchema);
    customerModel = mongoConnection.model(Customer.name, CustomerSchema);
    productModel = mongoConnection.model(Product.name, ProductSchema);

    const moduleProduct: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ProductController],
      providers: [
        ProductService,
        Logger,
        { provide: getModelToken(Product.name), useValue: productModel },
        { provide: getModelToken(Manga.name), useValue: productModel },
      ],
    }).compile();

    productController = moduleProduct.get<ProductController>(ProductController);

    const moduleCustomer: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [CustomerController],
      providers: [
        CustomerService,
        Logger,
        { provide: getModelToken(Customer.name), useValue: customerModel },
      ],
    }).compile();

    customerController =
      moduleCustomer.get<CustomerController>(CustomerController);

    const module: TestingModule = await Test.createTestingModule({
      controllers: [BoxController],
      providers: [
        BoxService,
        { provide: getModelToken(Box.name), useValue: boxModel },
      ],
    }).compile();

    controllerBox = module.get<BoxController>(BoxController);
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  afterEach(async () => {
    const collections = mongoConnection.collections;
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  });

  it('should be defined', () => {
    expect(controllerBox).toBeDefined();
    expect(customerController).toBeDefined();
    expect(productController).toBeDefined();
  });

  beforeEach(async () => {
    await customerController.create(giovanni);
    await productController.populate();
  });

  it('should add user', async () => {
    // await customerController.create(giovanni);
    expect((await customerController.findAll()).length).toBe(1);
  });

  it('should be 8 mangas', async () => {
    const mangas: Manga[] = await productController.findAll();
    expect(mangas.length).toBe(9);
  });

  it('should create box', async () => {
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const mangas: Manga[] = await productController.findAll();
    const justStartedManga: CustomerProduct = {
      currentID: 0,
      product: { title: mangas[0].title, image: mangas[0].image },
    };
    const box = await controllerBox.create(giovanni, justStartedManga);
    // await customerController.create(giovanni);
    expect(box).toBeDefined();
  }, 10_000);

  it('should create user with empty box', async () => {
    // const mangas: Manga[] = await productController.findAll();
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const box = await controllerBox.create(giovanni);
    expect(box).toBeDefined();
  }, 10_000);

  it('should find user with empty box', async () => {
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const box = await controllerBox.create(giovanni);
    expect(box).toBeDefined();
    expect(await controllerBox.findOne(giovanni.id)).toBeDefined();
  }, 10_000);

  it('should update empty box', async () => {
    // const mangas: Manga[] = await productController.findAll();
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const box = await controllerBox.create(giovanni);
    const mangas: Manga[] = await productController.findAll();
    const justStartedManga: CustomerProduct = {
      currentID: 0,
      product: { title: mangas[0].title, image: mangas[0].image },
    };
    await controllerBox.updateBox({ boxId: box.id }, justStartedManga);
    const boxes = await controllerBox.findAll();
    expect(boxes[0].customerProducts.length).toBe(1);
  }, 10_000);

  it('should update empty box with a lot', async () => {
    // const mangas: Manga[] = await productController.findAll();
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const box = await controllerBox.create(giovanni);
    const mangas: Manga[] = await productController.findAll();
    const justStartedManga: CustomerProduct[] = mangas.map((manga) => {
      return {
        currentID: 69,
        product: { title: manga.title, image: manga.image },
      };
    });
    for (const cProd of justStartedManga) {
      await controllerBox.updateBox({ boxId: box.id }, cProd);
    }
    const boxes = await controllerBox.findAll();
    expect(boxes[0].customerProducts.length).toBe(9);
  }, 10_000);

  it('should increment currentID in box', async () => {
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const mangas: Manga[] = await productController.findAll();
    const justStartedManga: CustomerProduct = {
      currentID: 0,
      product: { title: mangas[0].title, image: mangas[0].image },
    };
    const box = await controllerBox.create(giovanni, justStartedManga);
    // await customerController.create(giovanni);
    expect(box).toBeDefined();

    await controllerBox.increaseStatus(box.id, mangas[0].title);
    expect(await controllerBox.findOne(giovanni.id)).toBe(0);
  }, 10_000);

  it('should delete manga from box', async () => {
    const giovanni: CustomerDocument = await customerController.findOne(
      'mttdggvvv',
    );
    const mangas: Manga[] = await productController.findAll();
    const justStartedManga: CustomerProduct = {
      currentID: 0,
      product: { title: mangas[0].title, image: mangas[0].image },
    };
    const box = await controllerBox.create(giovanni, justStartedManga);
    expect(box).toBeDefined();
    await controllerBox.deleteProduct(box.id, mangas[0].title);
    expect(box).toBe(1);
  }, 10_000);
});
