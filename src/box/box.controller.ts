import { Body, Controller, Get, Logger, Param, Post } from '@nestjs/common';
import { Box, BoxDocument } from '../schemas/Box/box.schema';
import {
  Customer,
  CustomerDocument,
} from '../schemas/Customer/customer.schema';
import { CustomerProduct } from '../schemas/CustomerProduct/customer-product.schema';
import { BoxService } from './box.service';

@Controller('box')
export class BoxController {
  constructor(private readonly boxService: BoxService) {}

  init(funName: string, body?: any) {
    Logger.debug(`[${BoxController.name}] Calling ${funName} with: `);
    if (body) {
      Logger.debug(body);
    }
  }

  @Post()
  async create(
    @Body('customer') customer: CustomerDocument,
    @Body('customerProduct') product?: CustomerProduct,
  ): Promise<BoxDocument> {
    this.init(this.create.name, { customer: customer, custProd: product });
    return this.boxService.create(customer, product);
  }

  @Get()
  async findAll(): Promise<Box[]> {
    this.init(this.findAll.name);
    return this.boxService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Box> {
    this.init(this.findOne.name, id);
    return this.boxService.findOne(id);
  }

  @Post('/delete:boxId') //TODO
  deleteProduct(
    @Param('boxId') boxId: string,
    @Body('title') title: string,
    @Body('currentID') currentID: number,
  ) {
    this.init(this.deleteProduct.name, {
      param: boxId,
      body: { title, currentID },
    });
    return this.boxService.deleteProductFromBox(
      { boxId: boxId },
      title,
      currentID,
    );
  }

  @Post('/update:boxId')
  increaseStatus(@Param('boxId') boxId: string, @Body('title') title: string) {
    this.init(this.increaseStatus.name, { param: boxId, body: title });
    return this.boxService.updateCustomProd({ boxId: boxId }, title);
  }

  @Post(':boxId')
  async updateBox(
    @Param('boxId') boxId: any,
    @Body('customerProduct') product: CustomerProduct,
  ): Promise<Box> {
    this.init(this.updateBox.name, { param: boxId, body: product });
    return this.boxService.updateBox({ boxId: boxId }, product);
  }
}
