import { Module } from '@nestjs/common';
import { BoxService } from './box.service';
import { BoxController } from './box.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Box, BoxSchema } from '../schemas/Box/box.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Box.name,
        schema: BoxSchema,
      },
    ]),
  ],
  providers: [BoxService],
  controllers: [BoxController],
})
export class BoxModule {}
