import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { Box, BoxDocument, BoxSchema } from '../schemas/Box/box.schema';
import {
  Customer,
  CustomerDocument,
} from '../schemas/Customer/customer.schema';
import { CustomerProduct } from '../schemas/CustomerProduct/customer-product.schema';

@Injectable()
export class BoxService {
  constructor(@InjectModel(Box.name) private readonly boxModel: Model<Box>) {}

  async create(
    customer: CustomerDocument,
    product?: CustomerProduct,
  ): Promise<BoxDocument> {
    // const boxM = mongoose.model(Box.name, BoxSchema);
    // const newBox = new boxM({
    //   customer: customer,
    //   customerProduct: [product],
    // });
    // return await newBox.save();
    const b: Box = {
      customer: customer.id,
      customerProducts: product ? [product] : [],
    };
    return await this.boxModel.create(b);
  }

  findAll(): Promise<Box[]> {
    return this.boxModel.find().exec();
  }

  async findOne(id: string): Promise<Box> {
    return this.boxModel.findOne({ customer: id }).exec();
  }

  async updateBox(boxId: any, product: CustomerProduct): Promise<Box> {
    // return this.boxModel.updateOne({ customerProducts: box.customerProducts });
    return await this.boxModel.findByIdAndUpdate(boxId.boxId, {
      // customerProducts: [product],
      $push: { customerProducts: product },
    });
  }

  async deleteProductFromBox(boxId: any, title: string, currentID: number) {
    const box: Box = await this.boxModel.findById(boxId.boxId);
    const filt = box.customerProducts.filter(
      (cProd) => cProd.product.title != title || cProd.currentID != currentID,
    );

    return await this.boxModel.findByIdAndUpdate(boxId.boxId, {
      customerProducts: filt,
    });

    //! non vuole andare in nessun modo.... ho dovuto improvvisare
    // return await this.boxModel.findByIdAndUpdate(boxId.boxId, {
    //   $pull: { customerProducts: { product: { title: title } } },
    //   // $pull: 'customerProducts.$[elem]',
    // });
  }

  async updateCustomProd(boxId: any, title: string) {
    return await this.boxModel.findByIdAndUpdate(
      boxId.boxId,
      {
        // customerProducts: [product],
        // $inc: { customerProducts: { currentID: 1 } },
        $inc: { 'customerProducts.$[elem].currentID': 1 },
      },
      {
        arrayFilters: [{ 'elem.product.title': title }],
        // new: true,
      },
    );
    // return await this.boxModel.findByIdAndUpdate(boxId.boxId, {
    //   // customerProducts: [product],
    //   $inc: { customerProducts: { currentID: 1 } },
    // });
  }
  //   async delete(id: string) {
  //     const deletedCat = await this.customerModel
  //       .findByIdAndRemove({ _id: id })
  //       .exec();
  //     return deletedCat;
  //   }
}
