import { getModelToken } from '@nestjs/mongoose';
import { TestingModule, Test } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, Model, connect } from 'mongoose';
import { Product, ProductSchema } from '../schemas/Product/product.schema';
import { Logger } from '@nestjs/common';
import { HttpModule, HttpService } from '@nestjs/axios';
import { CustomerController } from './customer.controller';
import {
  Customer,
  CustomerDocument,
  CustomerSchema,
} from '../schemas/Customer/customer.schema';
import { CustomerService } from './customer.service';

const giovanni: Customer = {
  CF: 'mttdggvvv',
  name: 'Giovanni',
  surname: 'Di giovanni',
  telegramID: 'ksajdbf329db',
  email: 'digio@gmail.com',
  image:
    'https://images.pexels.com/photos/2698519/pexels-photo-2698519.jpeg?auto=compress&cs=tinysrgb&w=1600',
};

describe('CustomerController', () => {
  let controller: CustomerController;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let productModel: Model<Product>;
  let customerModel: Model<Customer>;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    productModel = mongoConnection.model(Product.name, ProductSchema);
    customerModel = mongoConnection.model(Customer.name, CustomerSchema);

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [CustomerController],
      providers: [
        CustomerService,
        Logger,
        { provide: getModelToken(Customer.name), useValue: customerModel },
      ],
    }).compile();

    controller = module.get<CustomerController>(CustomerController);
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  afterEach(async () => {
    const collections = mongoConnection.collections;
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  });

  // beforeEach(async () => {
  //   await controller.populate();
  // });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should add giovanni ! ', async () => {
    await controller.create(giovanni);
    const customers: CustomerDocument[] = await controller.findAll();
    expect(customers.length).toBe(1);
  });

  it('should add giovanni and find him! ', async () => {
    await controller.create(giovanni);
    const customer: CustomerDocument = await controller.findOne('mttdggvvv');
    expect(customer.CF).toBeDefined();
  });

  it('should find giovanni by his id! ', async () => {
    await controller.create(giovanni);
    const customer: string = (await controller.findOne('mttdggvvv')).id;
    const giova: Customer = await controller.findById(customer);
    expect(giova.CF).toBeDefined();
  });
  // it('should be 8 mangas', async () => {
  //   const mangas: Manga[] = await controller.findAll();
  //   expect(mangas.length).toBe(9);
  // });

  // it('should be add konosuba! ', async () => {
  //   await controller.create({ title: 'kono_subarashii_sekai_ni_shukufuku_o' });
  //   const mangas: Manga[] = await controller.findAll();
  //   expect(mangas.length).toBe(10);
  // });

  // it('should be add konosuba with image! ', async () => {
  //   const konosuba = await controller.create({
  //     title: 'kono_subarashii_sekai_ni_shukufuku_o',
  //     image: 'https://cdn.myanimelist.net/images/anime/10/81311l.jpg',
  //   });
  //   console.log(konosuba);
  //   const mangas: Manga[] = await controller.findAll();
  //   expect(mangas.length).toBe(10);
  // });

  // it('should get call of the night on the title ! ', async () => {
  //   const callOfTheNight = await controller.getProduct({
  //     title: 'Yofukashi no uta',
  //   });
  //   console.log(callOfTheNight);
  //   // await controller.create({ title: 'kono_subarashii_sekai_ni_shukufuku_o' });
  //   // const mangas: Manga[] = await controller.findAll();
  //   expect(callOfTheNight).toBeDefined();
  // });

  //   it('shoud add Call of the night', async () => {
  //     const box = await controller.create(giovanni, callOfTheNight);
  //     expect(box['_id']).toBeDefined();
  //   });
});
