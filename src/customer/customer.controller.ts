import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { CustomerDto } from '../schemas/Customer/customer.dto';
import {
  Customer,
  CustomerDocument,
} from '../schemas/Customer/customer.schema';
import { CustomerService } from './customer.service';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  init(funName: string, body?: any) {
    // Logger.warn(`Calling ${this.init.caller.name} with: `);
    Logger.debug(`[${CustomerController.name}] Calling ${funName} with: `);
    if (body) {
      Logger.debug(body);
    }
  }

  @Post()
  async create(@Body() createCustomerDto: CustomerDto): Promise<Customer> {
    this.init(this.create.name, createCustomerDto);
    return this.customerService.create(createCustomerDto);
  }

  @Get()
  async findAll(): Promise<CustomerDocument[]> {
    this.init(this.findAll.name);
    return this.customerService.findAll();
  }

  @Get('id')
  findById(@Param('id') customerId: string): PromiseLike<Customer> {
    this.init(this.findById.name, customerId);
    return this.customerService.findById(customerId);
  }

  @Get(':name')
  async findOne(@Param('name') name: string): Promise<CustomerDocument> {
    this.init(this.findOne.name, name);
    return this.customerService.findOne(name);
  }

  @Patch(':id')
  async updateCustomer(
    @Param('id') id: string,
    @Body('customer') value: any,
  ): Promise<Customer> {
    this.init(this.updateCustomer.name, { param: id, body: value });
    return this.customerService.update(id, value);
  }

  @Delete(':id')
  async delete(@Param('id') id: string): Promise<Customer> {
    this.init(this.delete.name, id);
    return this.customerService.delete(id);
  }
}
