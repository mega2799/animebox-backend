import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CustomerDto } from '../schemas/Customer/customer.dto';
import {
  Customer,
  CustomerDocument,
} from '../schemas/Customer/customer.schema';
import * as dotenv from 'dotenv';

@Injectable()
export class CustomerService {
  async update(id: string, value: any): Promise<Customer> {
    delete value['_id']; //! 0 trust in users.....
    return await this.customerModel.findByIdAndUpdate(id, value);
  }
  constructor(
    @InjectModel(Customer.name) private readonly customerModel: Model<Customer>,
  ) {
    dotenv.config();
  }

  async create(customer: CustomerDto): Promise<Customer> {
    const b: any[] = JSON.parse(process.env.LOGO_IMAGES);
    customer.image = b[Math.floor(Math.random() * b.length)];
    const createdCat = await this.customerModel.create(customer);
    return createdCat;
  }

  async findAll(): Promise<CustomerDocument[]> {
    return this.customerModel.find().exec();
  }

  findById(customerId: string): Promise<Customer> {
    return this.customerModel.findById(customerId).exec();
  }

  async findOne(custName: string): Promise<CustomerDocument> {
    return this.customerModel.findOne({ CF: custName }).exec();
  }

  async delete(id: string) {
    const deletedCat = await this.customerModel
      .findByIdAndRemove({ _id: id })
      .exec();
    return deletedCat;
  }

  async updateTelegramID(CF, telegramID) {
    const customer: CustomerDocument = await this.findOne(CF);
    console.log(customer);

    return await this.customerModel.findByIdAndUpdate(customer.id, {
      telegramID: telegramID,
    });
  }
}
