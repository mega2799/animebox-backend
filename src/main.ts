import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { AppService } from './app.service';
import { serialize } from 'v8';
import { CustomerService } from './customer/customer.service';
import { ProductService } from './product/product.service';
import { BoxService } from './box/box.service';
import { TelegramService } from './telegram/telegram.service';
import { Anime } from './schemas/Anime/anime.schema';
import { AnimeService } from './anime/anime.service';
const cron = require('node-cron');

async function bootstrap() {
  dotenv.config();
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const serv = app.get<AppService>(AppService);
  await app.listen(process.env.PORT);
  cron.schedule('0 8-22 * * *', async () => {
    // console.log('---------------------');
    // console.log('running a task every 15 seconds');

    serv.setCronJob(
      await app.resolve(CustomerService),
      await app.resolve(AnimeService),
      await app.resolve(ProductService),
      await app.resolve(BoxService),
      await app.resolve(TelegramService),
    );
  });
  // setInterval(async () => {
  //   serv.setCronJob(
  //     await app.resolve(CustomerService),
  //     await app.resolve(AnimeService),
  //     await app.resolve(ProductService),
  //     await app.resolve(BoxService),
  //     await app.resolve(TelegramService),
  //   );
  // }, 10_000);
}

bootstrap();
