export interface MangaComparison {
  title: string;
  searchID: string;
  latestVol: number;
}
