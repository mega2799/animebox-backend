export interface MangaHereDTO {
  id: string;
  title: string;
  description: string;
  headers: {
    Referer: string;
  };
  image: string;
  genres: string[];
  status: 'Ongoing' | 'Completed';
  rating: number;
  authors: string[];
  chapters: Chapter[];
}

export interface Chapter {
  id: string;
  title: string;
  releasedDate: string;
}
