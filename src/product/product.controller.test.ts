import { ProductController } from './product.controller';
import { getModelToken } from '@nestjs/mongoose';
import { TestingModule, Test } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, Model, connect } from 'mongoose';
import { Product, ProductSchema } from '../schemas/Product/product.schema';
import { ProductService } from './product.service';
import { Logger } from '@nestjs/common';
import { Manga, MangaSchema } from '../schemas/Manga/manga.schema';
import { HttpModule, HttpService } from '@nestjs/axios';

describe('BoxController', () => {
  let controller: ProductController;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let productModel: Model<Product>;
  let mangaModel: Model<Manga>;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    productModel = mongoConnection.model(Product.name, ProductSchema);
    mangaModel = mongoConnection.model(Manga.name, MangaSchema);

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [ProductController],
      providers: [
        ProductService,
        Logger,
        { provide: getModelToken(Product.name), useValue: productModel },
        { provide: getModelToken(Manga.name), useValue: productModel },
      ],
    }).compile();

    controller = module.get<ProductController>(ProductController);
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  afterEach(async () => {
    const collections = mongoConnection.collections;
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  });

  beforeEach(async () => {
    await controller.populate();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should be 8 mangas', async () => {
    const mangas: Manga[] = await controller.findAll();
    expect(mangas.length).toBe(9);
  });

  it('should be add konosuba! ', async () => {
    await controller.create({ title: 'kono_subarashii_sekai_ni_shukufuku_o' });
    const mangas: Manga[] = await controller.findAll();
    expect(mangas.length).toBe(10);
  });

  it('should be add konosuba with image! ', async () => {
    const konosuba = await controller.create({
      title: 'kono_subarashii_sekai_ni_shukufuku_o',
      image: 'https://cdn.myanimelist.net/images/anime/10/81311l.jpg',
    });
    console.log(konosuba);
    const mangas: Manga[] = await controller.findAll();
    expect(mangas.length).toBe(10);
  });

  it('should get call of the night on the title ! ', async () => {
    const callOfTheNight = await controller.getProduct({
      title: 'Yofukashi no uta',
    });
    console.log(callOfTheNight);
    // await controller.create({ title: 'kono_subarashii_sekai_ni_shukufuku_o' });
    // const mangas: Manga[] = await controller.findAll();
    expect(callOfTheNight).toBeDefined();
  });

  //   it('shoud add Call of the night', async () => {
  //     const box = await controller.create(giovanni, callOfTheNight);
  //     expect(box['_id']).toBeDefined();
  //   });
});
