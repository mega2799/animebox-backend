import { Body, Controller, Get, Logger, Param, Post } from '@nestjs/common';
import { ProductService } from './product.service';
import { Product } from '../schemas/Product/product.schema';
import { Manga } from 'src/schemas/Manga/manga.schema';

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get(':title')
  getProduct(@Param() title): Promise<Manga> {
    this.init(this.getProduct.name, title);
    return this.productService.find(title);
  }

  init(funName: string, body?: any) {
    // Logger.warn(`Calling ${this.init.caller.name} with: `);
    Logger.debug(`[${ProductController.name}] Calling ${funName} with: `);
    if (body) {
      Logger.debug(body);
    }
  }

  @Get('/import/manga')
  async populate() {
    this.init(this.populate.name);
    return this.productService.populateDB();
  }

  @Get()
  async findAll(): Promise<Manga[]> {
    this.init(this.findAll.name);
    return this.productService.findAll();
  }

  @Post()
  async create(@Body() newEntry: Product): Promise<Manga> {
    this.init(this.create.name, newEntry);
    // this.init(this.create.name, createUserDto);
    return await this.productService.create(newEntry);
  }
}
