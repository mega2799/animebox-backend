import { Logger, Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Product, ProductSchema } from 'src/schemas/Product/product.schema';
import { HttpModule } from '@nestjs/axios';
import { Manga, MangaSchema } from 'src/schemas/Manga/manga.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Manga.name, schema: MangaSchema }]),
    HttpModule,
  ],
  providers: [ProductService, Logger],
  controllers: [ProductController],
})
export class ProductModule {}
