import { HttpService } from '@nestjs/axios';
import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as fs from 'fs';
import { Model } from 'mongoose';
import * as path from 'path';
import { firstValueFrom, single } from 'rxjs';
import { MangaHereDTO } from './dto/mangahere.dto';
import { MangaDto, Volume } from '../schemas/Manga/manga.dto';
import { Manga, MangaDocument } from '../schemas/Manga/manga.schema';
import { Product } from 'src/schemas/Product/product.schema';
import { MangaComparison } from './dto/analyzer.dto';
import * as dotenv from 'dotenv';
dotenv.config()

// dotenv.config();
const assetPath = process.cwd() + path.sep + 'asset' + path.sep;
// const url = 'http://localhost:4300/manga/mangahere/info';
const url = process.env.MANGA_API;

@Injectable()
export class ProductService {
  constructor(
    @Inject(Logger) private readonly logger: Logger,
    @InjectModel(Manga.name) private readonly productModel: Model<Manga>,
    private readonly httpService: HttpService,
  ) {}

  async create(mangaTitle: Product): Promise<Manga> {
    const { data } = await firstValueFrom(
      await this.httpService.get<MangaHereDTO>(url, {
        params: {
          id: mangaTitle.title,
        },
      }),
    );

    if (!data.title) return undefined;

    const newManga: MangaDto = {
      title: data.title,
      description: data.description,
      image: mangaTitle.image ? mangaTitle.image : data.image,
      genres: data.genres,
      status: data.status,
      rating: data.rating,
      authors: data.authors,
      searchID: data.id,
      volumes: data.chapters.map((chap) => ({
        number: parseInt(chap.title.split('Ch.')[1]),
        released: new Date(Date.parse(chap.releasedDate)),
      })),
    };
    return await this.productModel.create(newManga);
  }

  async find(t: any) {
    return await this.productModel.findOne(t).exec();
  }

  async findAll(): Promise<MangaDocument[]> {
    //TODO fatto di fretta
    return await this.productModel.find().exec();
  }

  async getLatestVolumes() {
    const mangalist: MangaDocument[] = await this.findAll();
    const localLatestManga: MangaComparison[] = mangalist.map((manga) => {
      const latestVolumeNumber = Math.max(
        ...manga.volumes.map((vol: Volume) => vol.number),
      );
      return {
        title: manga.title,
        searchID: manga.searchID,
        latestVol: latestVolumeNumber,
      };
    });
    const remoteLatestManga: MangaComparison[] = [];
    const remoteMangaList: Manga[] = [];

    const toUpdateMangaID: any[] = [];
    for (const localManga of localLatestManga) {
      const { data } = await firstValueFrom(
        await this.httpService.get<MangaHereDTO>(url, {
          params: {
            id: localManga.searchID,
          },
        }),
      );
      const remoteManga: Manga = {
        title: data.title,
        description: data.description,
        image: data.image,
        genres: data.genres,
        status: data.status,
        rating: data.rating,
        authors: data.authors,
        searchID: data.id,
        volumes: data.chapters.map((chap) => ({
          number: parseInt(chap.title.split('Ch.')[1]),
          released: new Date(Date.parse(chap.releasedDate)),
        })),
      };

      remoteMangaList.push(remoteManga);
      const latestVolumeNumber = Math.max(
        ...remoteManga.volumes.map((vol: Volume) => vol.number),
      );
      remoteLatestManga.push({
        title: remoteManga.title,
        searchID: remoteManga.searchID,
        latestVol: latestVolumeNumber,
      });
    }

    for (const localManga of localLatestManga) {
      const remote: MangaComparison = remoteLatestManga.find(
        (val) => val.searchID === localManga.searchID,
      );
      if (remote.latestVol > localManga.latestVol) {
        toUpdateMangaID.push({
          title: localManga.title,
          latest: remote.latestVol,
        });
      }
    }

    this.logger.debug(
      `There are new volumes for ${
        toUpdateMangaID.length > 0 ? toUpdateMangaID : 'None'
      }`,
    );
    for (const singleMangaToUpdate of toUpdateMangaID) {
      const newVolumes = remoteMangaList.find(
        (val) => val.title === singleMangaToUpdate.title,
      ).volumes;
      const local = mangalist.find(
        (manga) => manga.title === singleMangaToUpdate.title,
      );

      await this.productModel.findByIdAndUpdate(local.id, {
        volumes: newVolumes,
      });
    }

    return toUpdateMangaID;
  }
  async populateDB() {
    const mangaList: any[] = JSON.parse(
      fs.readFileSync(assetPath + 'mangaList.json', 'utf-8'),
    );

    const mangas: MangaHereDTO[] = [];

    for (const singleManga of mangaList) {
      const { data } = await firstValueFrom(
        await this.httpService.get<MangaHereDTO>(url, {
          params: {
            id: singleManga.id,
          },
        }),
      );
      data.image = singleManga.image;
      mangas.push(data);
    }

    const volumes: MangaDto[] = mangas.map((manga) => {
      return {
        title: manga.title,
        description: manga.description,
        image: manga.image,
        genres: manga.genres,
        status: manga.status,
        rating: manga.rating,
        authors: manga.authors,
        searchID: manga.id,
        volumes: manga.chapters.map((chap) => ({
          number: parseInt(chap.title.split('Ch.')[1]),
          released: new Date(Date.parse(chap.releasedDate)),
        })),
      };
    });

    for (const volume of volumes) {
      await this.productModel.create(volume);
    }
    return true;
  }
}
