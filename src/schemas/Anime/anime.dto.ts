import { CreateProductDto } from '../Product/product.dto';

export class AnimeDto extends CreateProductDto {
  readonly mal_id: number;
  readonly url: string;
  readonly images: any;
  readonly titles: any[];
  readonly title: string;
  readonly title_english: string;
  readonly title_japanese: string;
  readonly episodes: number;
  readonly status: string;
  readonly airing: boolean;
  readonly aired: any;
  readonly duration: string;
}
