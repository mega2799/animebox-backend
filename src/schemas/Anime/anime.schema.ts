import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsNumber, IsString } from 'class-validator';
import { HydratedDocument } from 'mongoose';

export type AnimeDocument = HydratedDocument<Anime>;

@Schema()
export class Anime {
  @Prop()
  @IsNumber()
  readonly mal_id: number;
  @Prop()
  @IsString()
  readonly url: string;
  //   @Prop()
  //   readonly images: any;
  //   @Prop()
  //   readonly titles: any[];
  @Prop()
  readonly title: string;
  @Prop()
  readonly title_english: string;
  @Prop()
  readonly title_japanese: string;
  @Prop()
  readonly episodes: number;
  @Prop()
  readonly status: string;
  @Prop()
  readonly airing: boolean;
    // @Prop()
    // readonly aired: any[];
  @Prop()
  readonly duration: string;
}

export const AnimeSchema = SchemaFactory.createForClass(Anime);
