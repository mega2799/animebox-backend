import { Customer } from '../Customer/customer.schema';
import { CustomerProduct } from '../CustomerProduct/customer-product.schema';

export class CreateBoxDto {
  readonly customer: Customer;
  readonly customerProducts: CustomerProduct[];
}
