import { Param } from '@nestjs/common';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsArray, IsString, ValidateNested } from 'class-validator';
import { HydratedDocument, ObjectId, SchemaTypes } from 'mongoose';
import { Product } from '../Product/product.schema';
import { Type } from 'class-transformer';
import { Customer, CustomerSchema } from '../Customer/customer.schema';
import { CustomerProduct } from '../CustomerProduct/customer-product.schema';

export type BoxDocument = HydratedDocument<Box>;

@Schema()
export class Box {
  // @ValidateNested()
  // @Type(() => Customer)
  @Prop({ type: SchemaTypes.ObjectId })
  readonly customer: ObjectId;

  @Prop()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CustomerProduct)
  readonly customerProducts: CustomerProduct[];
}

export const BoxSchema = SchemaFactory.createForClass(Box);
