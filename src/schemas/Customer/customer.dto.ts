export class CustomerDto {
  readonly CF: string;
  readonly name: string;
  readonly surname: string;
  readonly telegramID: string;
  readonly email: string;
  image: string;
}
