import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsEmail, IsString } from 'class-validator';
import { HydratedDocument } from 'mongoose';

export type CustomerDocument = HydratedDocument<Customer>;

@Schema()
export class Customer {
  @Prop()
  @IsString()
  readonly CF: string;

  @Prop()
  @IsString()
  readonly name: string;

  @Prop()
  @IsString()
  readonly surname: string;

  @Prop()
  @IsString()
  readonly telegramID: string;

  @Prop()
  @IsEmail()
  readonly email: string;

  @Prop()
  @IsString()
  image: string;
}

export const CustomerSchema = SchemaFactory.createForClass(Customer);
