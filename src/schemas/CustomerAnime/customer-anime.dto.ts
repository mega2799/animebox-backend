export class CreateCustomerAnimeDto {
  readonly animeID: number;
  readonly customerID: string;
  readonly animeTitle: string;
  readonly animeTitleEng: string;
}
