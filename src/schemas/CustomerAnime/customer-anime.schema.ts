import { Param } from '@nestjs/common';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsEmail, IsNumber, IsString, ValidateNested } from 'class-validator';
import { HydratedDocument } from 'mongoose';
import { Product } from '../Product/product.schema';
import { Type } from 'class-transformer';

export type CustomerAnimerDocument = HydratedDocument<CustomerAnime>;

@Schema()
export class CustomerAnime {
  @Prop()
  @IsString()
  readonly animeID: number;

  @Prop()
  @IsString()
  readonly animeTitle: string;

  @Prop()
  @IsString()
  readonly animeTitleEng: string;

  @Prop()
  @IsString()
  readonly customerID: string;

  @Prop()
  @IsString()
  readonly currentEP: number;
}

export const CustomerAnimeSchema =
  SchemaFactory.createForClass(CustomerAnime);
