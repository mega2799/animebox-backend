import { Product } from '../Product/product.schema';

export class CreateCustomerProductDto {
  readonly product: Product;
  readonly currentID: number;
}
