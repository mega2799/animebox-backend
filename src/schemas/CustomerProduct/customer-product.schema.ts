import { Param } from '@nestjs/common';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsEmail, IsNumber, IsString, ValidateNested } from 'class-validator';
import { HydratedDocument } from 'mongoose';
import { Product } from '../Product/product.schema';
import { Type } from 'class-transformer';

export type CustomerProductDocument = HydratedDocument<CustomerProduct>;

@Schema()
export class CustomerProduct {
  @Prop() //TODO check it works
  @ValidateNested()
  @Type(() => Product)
  readonly product: Product;

  @Prop()
  @IsString()
  readonly currentID: number;
}

export const CustomerProductSchema =
  SchemaFactory.createForClass(CustomerProduct);
