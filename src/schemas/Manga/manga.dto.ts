import { CreateProductDto } from '../Product/product.dto';
import { Product } from '../Product/product.schema';

export class MangaDto extends CreateProductDto {
  readonly description: string;

  readonly image: string;

  readonly genres: string[];

  readonly status: 'Ongoing' | 'Completed';

  readonly rating: number;

  readonly authors: string[];

  readonly searchID: string;

  readonly volumes: Volume[];
}

export class Volume {
  readonly number: number;
  readonly released: Date;
}
