import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import {
  IsArray,
  IsEnum,
  IsNumber,
  IsString,
  ValidateNested,
} from 'class-validator';
import { HydratedDocument } from 'mongoose';
import { Product } from '../Product/product.schema';
import { Type } from 'class-transformer';

export type MangaDocument = HydratedDocument<Manga>;

export class VolumeDto {
  @Prop()
  @IsNumber()
  readonly number: number;

  @Prop()
  @IsString()
  readonly released: Date;
}

@Schema()
export class Manga {
  @Prop()
  @IsString()
  readonly title: string;

  @Prop()
  @IsString()
  readonly description: string;

  @Prop()
  @IsNumber()
  @IsString()
  readonly image: string;

  @Prop()
  @IsArray()
  @IsString({ each: true })
  readonly genres: string[];

  @Prop()
  @IsEnum(['Ongoing', 'Completed'])
  readonly status: 'Ongoing' | 'Completed';

  @Prop()
  @IsNumber()
  readonly rating: number;

  @Prop()
  @IsArray()
  @IsString({ each: true })
  readonly authors: string[];

  @Prop()
  @IsString()
  readonly searchID: string;

  @Prop()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => VolumeDto)
  readonly volumes: VolumeDto[];
}

export const MangaSchema = SchemaFactory.createForClass(Manga);
