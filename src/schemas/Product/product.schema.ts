import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsString } from 'class-validator';
import { HydratedDocument } from 'mongoose';

export type ProductDocument = HydratedDocument<Product>;

@Schema()
export class Product {
  @Prop()
  @IsString()
  readonly title: string;

  @Prop()
  @IsString()
  readonly image?: string;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
