import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IsString } from 'class-validator';
import { HydratedDocument } from 'mongoose';

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User {
  @Prop()
  @IsString()
  username: string;

  @Prop()
  @IsString()
  password: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
