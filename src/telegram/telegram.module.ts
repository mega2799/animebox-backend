import { Logger, Module } from '@nestjs/common';
import { TelegramService } from './telegram.service';
import { CustomerModule } from 'src/customer/customer.module';
import { CustomerService } from 'src/customer/customer.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Customer, CustomerSchema } from 'src/schemas/Customer/customer.schema';
import { AnimeService } from 'src/anime/anime.service';
import { AnimeModule } from 'src/anime/anime.module';
import { HttpModule } from '@nestjs/axios';
import { Anime, AnimeSchema } from 'src/schemas/Anime/anime.schema';
import {
  CustomerAnime,
  CustomerAnimeSchema,
} from 'src/schemas/CustomerAnime/customer-anime.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Customer.name, schema: CustomerSchema },
    ]),
    MongooseModule.forFeature([
      { name: Anime.name, schema: AnimeSchema },
      { name: CustomerAnime.name, schema: CustomerAnimeSchema },
    ]),
    HttpModule,

    // HttpModule,
    AnimeModule,
  ],
  providers: [TelegramService, CustomerService, AnimeService],
})
export class TelegramModule {}
