import { Injectable, Logger } from '@nestjs/common';

import * as tf from 'telegraf';
import * as dotenv from 'dotenv'; // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
import { CustomerService } from 'src/customer/customer.service';
import { from } from 'rxjs';
import { Customer } from 'src/schemas/Customer/customer.schema';
import { AnimeService } from 'src/anime/anime.service';
dotenv.config();
const bot = new tf.Telegraf(`${process.env.TOKEN}`);

@Injectable()
export class TelegramService {
  constructor(
    private readonly customerService: CustomerService,
    private readonly animeService: AnimeService,
  ) {
    this.setupHello(bot);
    this.handle(bot);
    bot.launch();
  }
  // async notify(user: Customer, manga, image) {
  //   Logger.verbose(manga);
  //   // const what = await bot.telegram.sendMessage(
  //   //   user.telegramID,
  //   //   `Hi ${user.name} The volume ${manga.currentID} of ${manga.product.title} is in store now !\n`,
  //   // );
  //   await bot.telegram.sendPhoto(user.telegramID, image, {
  //     caption: `Hi ${user.name} The volume ${manga.latest} of ${manga.title} is in store now !\n`,
  //   });
  // }
  async notify(userID: string, animeTitle: string, episodeNumber) {
    const users = await this.customerService.findAll();
    const user = users.find((user) => user.telegramID == userID);
    if (user && user.name) {
      console.log(
        'Notifying ',
        userID,
        `aka ${user.name}`,
        animeTitle,
        episodeNumber,
      );
      await bot.telegram.sendMessage(
        userID,
        `Hi ${user.name} ${episodeNumber} of ${animeTitle} is out now !\n`,
      );
    } else {
      console.log('Notifying ', userID, animeTitle, episodeNumber);
      await bot.telegram.sendMessage(
        userID,
        `${episodeNumber} of ${animeTitle} is out now !\n`,
      );
    }
  }

  async handle(bot: tf.Telegraf<tf.Context<import('typegram').Update>>) {
    bot.command('register', async (ctx) => {
      const CF = ctx.message.text.split('/register ')[1];

      this.customerService.create({
        CF,
        telegramID: `${ctx.from.id}`,
        name: CF,
        surname: CF,
        email: '',
        image: '',
      });
      // const updated = await this.customerService.updateTelegramID(
      //   CF,
      //   ctx.from.id,
      // );
      // console.log(updated);

      bot.telegram.sendMessage(
        ctx.chat.id,
        `Welcome to your new personal anime notifier \n /search followed by a space and the title of the anime you want to search \n Es; /search one piece`,
        {},
      );
    });

    bot.command('search', async (ctx) => {
      const animeTitle = ctx.message.text.split('/search')[1];
      bot.telegram.sendMessage(ctx.chat.id, `Just a second...`, {});

      const animes = await this.animeService.search(animeTitle);

      const keyboard = animes.map((anime) => {
        return [
          {
            text: anime.title_english ? anime.title_english : anime.title,
            callback_data: anime.mal_id.toString(),
          },
        ];
      });

      const replyMarkup = {
        inline_keyboard: keyboard,
      };

      bot.telegram.sendMessage(
        ctx.chat.id,
        `Here are the results of your search`,
        {
          reply_markup: replyMarkup,
        },
      );
    });
    // bot.action(/press:(\d+)/, (ctx) => {
    //   // Estrai l'id dell'anime dalla callback_data
    //   const animeId = ctx.match[1];
    //   ctx.reply(`Hai premuto il pulsante con l'id ${animeId}`);
    //   // Esegui le azioni desiderate in base all'id dell'anime
    // });
    bot.on('callback_query', async (ctx) => {
      const callbackData: any = ctx.callbackQuery; //message.text.split('/search')[1];
      if (!callbackData.data) Logger.error('No data');
      await this.animeService.addToList(callbackData.data, `${ctx.from.id}`);
    });
  }

  public async setupHello(
    bot: tf.Telegraf<tf.Context<import('typegram').Update>>,
  ) {
    bot.command('start', async (ctx) => {
      console.log(ctx.from);
      console.log(ctx);
      bot.telegram.sendMessage(
        ctx.chat.id,
        `Welcome to anime notifier bot type /register followed by a space and your name \n Es; /register xxxAnime-lover-69xxx`,
        {},
      );
    });
  }
}
